# type-III

### Event Generation

## get job option files from git

<pre>
setupATLAS
lstup git
git clone https://:@gitlab.cern.ch:8443/atlas-physics/pmg/infrastructure/mc15joboptions.git
</pre>

W/Z/h hadronic decay samples are not official samples
modify some samples from official samples, for example MC15.310709.MGPy8EG_N30LO_A14N23LO_TypeIIISeesaw_M900_2l_0tauhad.py

## Generate samples

<pre>
asetup 19.2.5.36,AtlasProduction,here
Generate_tf.py --ecmEnergy 13000 --firstEvent 1 --randomSeed 0 --jobConfig MC15.600000.MGPy8EG_N30LO_A14N23LO_TypeIIISeesaw_M900_0l_0tauhad.py --outputEVNTFile EVNT.pool.root --runNumber 600000 --maxEvents 10
</pre>

submit Grid
<pre>
asetup 19.2.5.36,AtlasProduction,here
lsetup emi
lsetup pyami
lsetup panda
pathena --trf " Generate_tf.py --ecmEnergy 13000 --firstEvent %SKIPEVENTS --randomSeed %RNDM:0 --jobConfig MC15.600000.MGPy8EG_N30LO_A14N23LO_TypeIIISeesaw_M900_0l_0tauhad.py --outputEVNTFile %OUT.EVNT.pool.root --runNumber 600000 --maxEvents 2000 " --nEventsPerJob 500 --split 10 --athenaTag=19.2.5.36 --outDS=user.yokazaki.TypeIIISeesaw_M900_0l_0tauhad.EVGEN.0_0921/
</pre>

## Simulation

<pre>
asetup AtlasOffline 21.0.15
Sim_tf.py --DBRelease current --DataRunNumber 284500 --conditionsTag OFLCOND-MC16-SDR-14 --geometryVersion ATLAS-R2-2016-01-00-01_VALIDATION --physicsList FTFP_BERT_ATL_VALIDATION --postInclude RecJobTransforms/UseFrontier.py --preExec "EVNTtoHITS:simFlags.SimBarcodeOffset.set_Value_and_Lock(200000)" "EVNTtoHITS:simFlags.TRTRangeCut=30.0;simFlags.TightMuonStepping=True" --preInclude "EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py" --simulator FullG4  --truthStrategy MC15aPlus --inputEVNTFile EVNT.pool.root --outputHITSFile HITS.pool.root --maxEvents 100 --skipEvents 0 --randomSeed 0 --firstEvent 1
</pre>

submit Grid
<pre>
asetup AtlasOffline 21.0.15
lsetup emi
lsetup pyami
lsetup panda
pathena --trf " Sim_tf.py --physicsList 'FTFP_BERT_ATL_VALIDATION' --truthStrategy 'MC15aPlus' --simulator 'FullG4' --DBRelease 'all:current' --conditionsTag 'default:OFLCOND-MC16-SDR-14' --DataRunNumber '284500' --preExec 'EVNTtoHITS:simFlags.SimBarcodeOffset.set_Value_and_Lock(200000)' 'EVNTtoHITS:simFlags.TRTRangeCut=30.0;simFlags.TightMuonStepping=True' --preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py' --geometryVersion 'default:ATLAS-R2-2016-01-00-01_VALIDATION' --postInclude 'default:RecJobTransforms/UseFrontier.py' --inputEVNTFile %IN --outputHITSFile %OUT.HITS.pool.root --maxEvents -1 --skipEvents 0 --randomSeed 0 " --inDS=user.yokazaki.TypeIIISeesaw_M900_0l_0tauhad.EVGEN.0_0921_EXT0/ --athenaTag=21.0.15 --outDS=user.yokazaki.TypeIIISeesaw_M900_0l_0tauhad.HITS.1_0921/ --nFilesPerJob=1
pathena --trf " Sim_tf.py --AMI s3126 --inputEVNTFile %IN --outputHITSFile %OUT.HITS.pool.root --maxEvents -1 --skipEvents 0 --randomSeed %RNDM:0 " --inDS=user.yokazaki.TypeIIISeesaw_M900_0l_0tauhad.EVGEN.0_0924_EXT0/ --outDS=user.yokazaki.TypeIIISeesaw_M900_0l_0tauhad.HITS.1_0925/ --nFilesPerJob=1 --excludedSite=IFIC
</pre>

## Reconstruction

check options
<pre>
GetTfCommand.py --AMI r10724
</pre>

<pre>
asetup AtlasOffline 21.0.20
Reco_tf.py --digiSteeringConf 'StandardSignalOnlyTruth' --conditionsTag 'default:OFLCOND-MC16-SDR-20' --valid 'True' --pileupFinalBunch '6' --numberOfHighPtMinBias '0.2099789464' --autoConfiguration 'everything' --numberOfLowPtMinBias '80.290021063135' --steering 'doRDO_TRIG' --postExec 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"]' 'ESDtoAOD:fixedAttrib=[s if "CONTAINER_SPLITLEVEL = '99'" not in s else "" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib' --postInclude 'default:PyJobTransforms/UseFrontier.py' --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)' 'ESDtoAOD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock("AODSLIM");' 'all:from ParticleBuilderOptions.AODFlags import AODFlags; AODFlags.ThinInDetForwardTrackParticles.set_Value_and_Lock(True)' --triggerConfig 'RDOtoRDOTrigger=MCRECO:DBF:TRIGGERDBMC:2179,51,207' --geometryVersion 'default:ATLAS-R2-2016-01-00-01' --numberOfCavernBkg '0' --inputHITSFile HITS.pool.root --outputRDOFile RDO.pool.root --outputESDFile ESD.pool.root --outputAODFile AOD.pool.root --maxEvents 100 --jobNumber 1
pathena --trf " Reco_tf.py --AMI r9364 --inputHITSFile %IN --outputAODFile %OUT.AOD.pool.root --maxEvents 100 --skipEvents 0 --inputHighPtMinbiasHitsFile=%HIMBIN --inputLowPtMinbiasHitsFile=%LOMBIN --jobNumber=%RNDM:0 " --inDS=user.yokazaki.TypeIIISeesaw_M900_0l_0tauhad.HITS.1_0925_2_EXT0/ --outDS=user.yokazaki.TypeIIISeesaw_M900_0l_0tauhad.RECO.1_1002_11/ --athenaTag=AtlasOffline,21.0.20 --nFilesPerJob=1 --lowMinDS mc16_13TeV.361238.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_low.simul.HITS.e4981_s3087_s3111/ --highMinDS mc16_13TeV.361239.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_high.simul.HITS.e4981_s3087_s3111/ --nHighMin=1 --nLowMin=2
</pre>

## Deriviation

<pre>
mkdir DFSUSY_R21.2
cd DFSUSY_R21.2
asetup AthDerivation,21.2.79.0
lsetup git
git atlas init-workdir https://:@gitlab.cern.ch:8443/atlas/athena.git
git checkout release/21.2.79.0
git atlas addpkg DerivationFrameworkSUSY
cd ../
mkdir build; cd build
cmake ../athena/Projects/WorkDir
make
source x86_64-centos7-gcc62-opt/setup.sh
cd ../
mkdir run; cd run
Reco_tf.py --reductionConf 'SUSY1' --sharedWriter 'True' --preExec 'default:from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = "BTagCalibRUN12-08-49";from AthenaMP.AthenaMPFlags import jobproperties as ampjp;ampjp.AthenaMPFlags.UseSharedWriter=True;import AthenaPoolCnvSvc.AthenaPool;ServiceMgr.AthenaPoolCnvSvc.OutputMetadataContainer="MetaData";from AthenaCommon.AlgSequence import AlgSequence;topSequence = AlgSequence ();topSequence += CfgMgr.xAODMaker__DynVarFixerAlg("BTaggingELFixer", Containers = ["BTagging_AntiKt4EMTopoAux." ] );' --inputAODFile ../../AOD.pool.root --outputDAODFile DAOD.pool.root
</pre>

## Make Ntuple

<pre>
git clone https://gitlab.cern.ch/atlas-phys-susy-wg/Electroweak/fullyhadronic/Run_SusySkimEWKHad.git
cd Run_SusySkimEWKHad
git submodule update --init --recursive
</pre>

read README.md of Run_SusySkimEWKHad


